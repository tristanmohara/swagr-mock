from flask import Flask, abort
from flask import Flask, send_file, send_from_directory
from pip._vendor.pep517.compat import FileNotFoundError

app = Flask(__name__)


@app.route('/swag/showinarea')
def showInArea():
    return open("responses/showinarea.json", 'r').read()

@app.route('/swag/oneswag')
def showOneSwag():
    return open("responses/showoneswag.json", 'r').read()

@app.route('/swag/login')
def login():
    return open("responses/showoneswag.json", 'r').read()

@app.route('/swag/model')
def swagmodel():
    try:
        return send_from_directory('responses', 'can.obj', as_attachment=True)
    except FileNotFoundError:
        abort(404)

@app.route('/connection/test')
def connectiontest():
    return "SWAG'R"


if __name__ == '__main__':
    app.run()
